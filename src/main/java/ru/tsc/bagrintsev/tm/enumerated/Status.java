package ru.tsc.bagrintsev.tm.enumerated;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private final String displayName;

    public static Status toStatus(String value) {
        if (value == null || value.isEmpty()) return null;
        for (Status status : Status.values()) {
            if (status.toString().equals(value)) {
                return status;
            }
        }
        return null;
    }

    public static String toName(Status status) {
        if (status == null) return "";
        for (Status value : Status.values()) {
            if (value.equals(status)) {
                return status.getDisplayName();
            }
        }
        return null;
    }

    Status(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
