package ru.tsc.bagrintsev.tm.controller;

import ru.tsc.bagrintsev.tm.api.controller.ICommandController;
import ru.tsc.bagrintsev.tm.api.sevice.ICommandService;
import ru.tsc.bagrintsev.tm.model.Command;

import static ru.tsc.bagrintsev.tm.util.FormatUtil.formatBytes;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final String processors = String.format("Available processors (cores): %d", runtime.availableProcessors());
        final long freeMemoryLong = runtime.freeMemory();
        final String freeMemory = String.format("Free memory: %s", formatBytes(freeMemoryLong));
        final long maxMemoryLong = runtime.maxMemory();
        final boolean isMaximum = maxMemoryLong == Long.MAX_VALUE;
        final String maxMemoryStr = isMaximum ? "no limit" : formatBytes(maxMemoryLong);
        final String maxMemory = String.format("Maximum memory: %s", maxMemoryStr);
        final long totalMemoryLong = runtime.totalMemory();
        final String totalMemory = String.format("Total memory available to JVM: %s", formatBytes(totalMemoryLong));
        final long usedMemoryLong = totalMemoryLong - freeMemoryLong;
        final String usedMemory = String.format("Used memory in JVM: %s", formatBytes(usedMemoryLong));
        System.out.printf("%s\n%s\n%s\n%s\n%s\n", processors, maxMemory, totalMemory, freeMemory, usedMemory);
    }

    @Override
    public void showWelcome() {
        System.out.println("*** Welcome to Task Manager ***");
    }

    @Override
    public void showHelp() {
        System.out.println("[Supported commands]");
        System.out.println("[Command Line              | While Running                      ]");
        System.out.println("[--------------------------|------------------------------------]");
        Command[] repository = commandService.getAvailableCommands();
        for (Command command : repository) {
            System.out.println(command);
        }
    }

    @Override
    public void showCommands() {
        System.out.println("[Interaction commands]");
        Command[] repository = commandService.getAvailableCommands();
        for (Command command : repository) {
            String commandName = command.getCommandName();
            String description = command.getDescription();
            if (commandName == null || commandName.isEmpty()) continue;
            System.out.printf("%-35s%s\n", commandName, description);
        }
    }

    @Override
    public void showArguments() {
        System.out.println("[CommandLine arguments]");
        Command[] repository = commandService.getAvailableCommands();
        for (Command command : repository) {
            String argumentName = command.getArgumentName();
            String description = command.getDescription();
            if (argumentName == null || argumentName.isEmpty()) continue;
            System.out.printf("%-35s%s\n", argumentName, description);
        }
    }

    @Override
    public void showVersion() {
        System.out.println("task-manager version 1.13.0");
    }

    @Override
    public void showAbout() {
        System.out.println("[Author]");
        System.out.println("Name: Sergey Bagrintsev");
        System.out.println("E-mail: sbagrintsev@t1-consulting.com");
    }

    @Override
    public void showError(final String arg) {
        System.err.printf("Error! Command '%s' is not supported...\n", arg);
    }

    @Override
    public void showOnStartError(final String arg) {
        System.err.printf("Error! Argument '%s' is not supported...\n", arg);
    }

    @Override
    public void close() {
        System.exit(0);
    }


}
