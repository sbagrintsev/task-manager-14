package ru.tsc.bagrintsev.tm.api.model;

import java.util.Date;

public interface IHasDateStarted {

    Date getDateStarted();

    void setDateStarted(Date dateStarted);

}
