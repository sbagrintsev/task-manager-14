package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project create(String name);

    Project create(String name, String description);

    Project add(Project project);

    List<Project> findAll();

    List<Project> findAll(Sort sort);

    Project findOneByIndex(Integer index);

    Project findOneById(String id);

    Project updateByIndex(Integer index, String name, String description);

    Project updateById(String id, String name, String description);

    Project removeProjectByIndex(Integer index);

    Project removeProjectById(String id);

    Project remove(Project project);

    Project changeProjectStatusByIndex(Integer index, Status stauts);

    Project changeProjectStatusById(String id, Status status);

    void clear();

}
