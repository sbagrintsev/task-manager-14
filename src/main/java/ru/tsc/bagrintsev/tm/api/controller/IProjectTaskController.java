package ru.tsc.bagrintsev.tm.api.controller;

import java.io.IOException;

public interface IProjectTaskController {

    void bindTaskToProject() throws IOException;

    void unbindTaskFromProject() throws IOException;

}
