package ru.tsc.bagrintsev.tm.service;

import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.api.sevice.ITaskService;
import ru.tsc.bagrintsev.tm.comparator.DateCreatedComparator;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;


    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name);
    }

    @Override
    public Task create(final String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.create(name, description);
    }

    @Override
    public Task add(final Task task) {
        if (task == null) return null;
        return taskRepository.add(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Sort sort) {
        if (sort == null) {
            return taskRepository.findAll(DateCreatedComparator.INSTANCE);
        }
        return taskRepository.findAll(sort.getComparator());
    }

    @Override
    public Map<Integer, Task> findAllByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index == null || index < 0 || index > taskRepository.taskCount()) return null;
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task findOneById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findOneById(id);
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0 || index > taskRepository.taskCount()) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeTaskByIndex(final Integer index) {
        if (index == null || index < 0 || index > taskRepository.taskCount()) return null;
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task removeTaskById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.removeById(id);
    }

    @Override
    public Task remove(final Task task) {
        if (task == null) return null;
        return taskRepository.remove(task);
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index > taskRepository.taskCount()) return null;
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) {
            task.setDateStarted(new Date());
        } else if (status.equals(Status.COMPLETED)) {
            task.setDateFinished(new Date());
        } else if (status.equals(Status.NOT_STARTED)) {
            task.setDateStarted(null);
            task.setDateFinished(null);
        }
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) return null;
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) {
            task.setDateStarted(new Date());
        } else if (status.equals(Status.COMPLETED)) {
            task.setDateFinished(new Date());
        } else if (status.equals(Status.NOT_STARTED)) {
            task.setDateStarted(null);
            task.setDateFinished(null);
        }
        return task;
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }
}
