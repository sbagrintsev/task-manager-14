package ru.tsc.bagrintsev.tm.service;

import ru.tsc.bagrintsev.tm.api.repository.IProjectRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IProjectService;
import ru.tsc.bagrintsev.tm.comparator.DateCreatedComparator;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.model.Project;

import java.util.Date;
import java.util.List;

public class ProjectService implements IProjectService {


    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    @Override
    public Project create(final String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(name, description);
    }

    @Override
    public Project add(final Project project) {
        if (project == null) return null;
        return projectRepository.add(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Sort sort) {
        if (sort == null) {
            return projectRepository.findAll(DateCreatedComparator.INSTANCE);
        }
        return projectRepository.findAll(sort.getComparator());
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (index == null || index < 0 || index > projectRepository.projectCount()) return null;
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project findOneById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findOneById(id);
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0 || index > projectRepository.projectCount()) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeProjectByIndex(final Integer index) {
        if (index == null || index < 0 || index > projectRepository.projectCount()) return null;
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project removeProjectById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.removeById(id);
    }

    @Override
    public Project remove(final Project project) {
        if (project == null) return null;
        return projectRepository.remove(project);
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index > projectRepository.projectCount()) return null;
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) {
            project.setDateStarted(new Date());
        } else if (status.equals(Status.COMPLETED)) {
            project.setDateFinished(new Date());
        } else if (status.equals(Status.NOT_STARTED)) {
            project.setDateStarted(null);
            project.setDateFinished(null);
        }
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) return null;
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) {
            project.setDateStarted(new Date());
        } else if (status.equals(Status.COMPLETED)) {
            project.setDateFinished(new Date());
        } else if (status.equals(Status.NOT_STARTED)) {
            project.setDateStarted(null);
            project.setDateFinished(null);
        }
        return project;
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}
